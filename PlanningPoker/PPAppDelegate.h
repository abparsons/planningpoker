//
//  PPAppDelegate.h
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
