//
//  PPFullCardViewController.m
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import "PPFullCardViewController.h"

@interface PPFullCardViewController ()

@end

@implementation PPFullCardViewController

@synthesize card;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.value.text = card;
    self.valueLarge.text = card;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
