//
//  main.m
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PPAppDelegate class]));
    }
}
