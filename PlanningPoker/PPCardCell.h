//
//  PPCardCell.h
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPCardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *value;

@end
