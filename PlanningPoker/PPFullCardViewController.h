//
//  PPFullCardViewController.h
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPFullCardViewController : UIViewController {
    NSString *card;
}

@property (nonatomic, strong) NSString *card;

@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *valueLarge;

@end
