//
//  PPCardsViewController.m
//  PlanningPoker
//
//  Created by Andrew Parsons on 11/13/13.
//  Copyright (c) 2013 ABP. All rights reserved.
//

#import "PPCardsViewController.h"
#import "PPCardCell.h"
#import "PPFullCardViewController.h"

@interface PPCardsViewController ()

@end

@implementation PPCardsViewController {
    NSArray *pokerCards;
    UIImageView *cellBackgroundView;
    UIColor *cellBackgroundColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    cellBackgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"card-top-320.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cellBackgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"card-top-320.png"]];
    
    pokerCards = [NSArray arrayWithObjects:@"0", @"1", @"2", @"3", @"5", @"8", @"13", @"20", @"40", @"100", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [pokerCards count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PokerCardCell";
    PPCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.selectedBackgroundView =  cellBackgroundView;
    cell.backgroundColor = cellBackgroundColor;
    cell.backgroundView = cellBackgroundView;
    cell.value.text = [pokerCards objectAtIndex:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 81;
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowFullCard"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *card = [pokerCards objectAtIndex:indexPath.row];
        PPFullCardViewController *destViewController = segue.destinationViewController;
        destViewController.card = card;
    }
}

@end
